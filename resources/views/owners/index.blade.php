@extends('layouts/default')
@section('title')
    Owner Info
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}"/>
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/>
@stop

{{-- Page content --}}
@section('content')
<header class="head">
    <div class="main-bar row">
        <div class="col-lg-6 col-sm-4">
            <h4 class="nav_top_align">
                <i class="fa fa-users"></i>

            </h4>
        </div>

    </div>
</header>
<div class="outer">
<div class="inner bg-container">
<div class="card">
  <div class="card-header bg-warning">
      Owners
  </div>
  <div class="card-block m-t-35" id="user_body">
      <div class="table-toolbar">
          <div class="btn-group">
              <a href="{{route('owners.create')}} " id="editable_table_new" class=" btn btn-default">
                  Add Owner <i class="fa fa-plus"></i>
              </a>
          </div>
          <div class="btn-group float-xs-right users_grid_tools">
              <div class="tools"></div>
          </div>
      </div>
      <div>
          <div>
                <table class="table  table-striped table-bordered table-hover dataTable no-footer"
                         id="editable_table" data-order='[[ 5, "dec" ]]' role="grid">
                    <thead>
                    <tr>
                      <th>Puntland ID</th>
                      <th>Fisrt name</th>
                      <th>Second Name</th>
                      <th>Last Name</th>
                      <th>Phone</th>
                      <th >Created</th>
                      <th class="actions">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
          @foreach ($owners as $owner)
                    <tr>
                      <td>{{ $owner->pid }}</td>
                      <td>{{ $owner->fname }}</td>
                      <td>{{ $owner->sname }}</td>
                      <td>{{ $owner->tname }}</td>
                      <td>{{ $owner->phone }}</td>
                      <td>{{ $owner->created_at->format('d/m/Y') }}</td>
                      <td class="no-sort no-click" id="bread-actions">
                      <a href="{{route('owners.edit', [$owner->id])}}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                      <i class="fa fa-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                      </a>

                      <a href="{{route('owners.show', [$owner->id])}}" title="View" class="btn btn-sm btn-warning pull-right">
                      <i class="fa fa-eye"></i> <span class="hidden-xs hidden-sm">View</span>
                      </a>
                      </td>
                    </tr>
              @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        

    </div>
</div>
</div>
<!-- /.outer -->
</div>
    <!-- /.outer -->
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<!--Plugin scripts-->
   <script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.colVis.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.html5.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>
   <!--End of plugin scripts-->
   <!--Page level scripts-->
   <script type="text/javascript" src="{{asset('assets/js/pages/users.js')}}"></script>
   <!-- end page level scripts -->
@stop
