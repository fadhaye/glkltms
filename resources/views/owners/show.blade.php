@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Owner Info
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!--plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/scroller.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/dataTables.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/invoice.css')}}">

<!-- end of plugin styles -->
<!--Page level styles-->
<!--End of page level styles-->
<style media="screen">
  #pic{
    height:200px;
    width:300px;
  }
</style>
@stop


{{-- Page content --}}
@section('content')
    <header class="head">
        <div class="main-bar">
            <div class="row">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-home"></i>
                        Dashboard
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
      <div class="inner bg-light lter bg-container">
        <div class="row">

        <div class="col-12">
          @role('superadministrator|admin')
          <div class="btn-group">
          <a href="{{url('lands/create')}}?id={{$owner->id}}" class="btn btn-primary">
          Register New Land
          <i class="fa fa-plus"></i>
          </a>
          </div>
          @endrole
          <div class=" invoice_print float-right" >
                    <span class="pull-sm-right">
                        <a style="color:#fff;" class="btn button-alignment btn-info m-t-15" data-toggle="button" onclick="javascript:window.print();">
                            Print
                        </a>
                    </span>
          </div>
          <div class="btn-group float-right">
          </div>
        </div>
      </div>


<br>
      <div class="card">

        <h4 class="card-title"></h4>
        <div class="card-body">
      <div class="row">
        <div class="col-sm-3">
          <div class="card">
            <div class="card-header bg-info">
                <div>
                    <i class="fa fa-user"></i>
                    Owner Picture
                </div>
            </div>
            <div class="card-body">
                        <div class="pic">
                          <img src="/storage/{{$owner->photo}}" alt="" width="200" height="200">
                        </div>

            </div>
          </div>
        </div>
        <div class="col-sm-9">
          <div class="card ">

            <div class="card-header bg-info">
                <div>
                    <i class="fa fa-user"></i>
                    Owner Info (Registraton ID:{{ $owner->id }})
                </div>
            </div>

              <div class="bg-warning m-t-35 header_alignr">

              <h4 class="card-title">
                <table class="table"
                         id="editable_table" data-order='[[ 5, "dec" ]]' role="grid">                <tr>

                           <tr>
                             <th>ID:</th>
                             <td>{{ $owner->type }}: {{ $owner->pid }}</td>
                           </tr>
              <tr>
                  <th>Full name: </th>
                  <span>
                <td>{{ $owner->fname }}
                  {{ $owner->sname }}
                  {{ $owner->tname }}
                {{ $owner->fthname }}</td>
                  </tr></span>


              <tr>
                <th>Date of Birth:</th>
                <td>{{ $owner->dob }}</td>
            </tr>

            <tr>
              <th>Gender:</th>
              <td>{{ $owner->gender }}</td>
          </tr>
          <tr>
            <th>Mother's Name:</th>
            <td>{{ $owner->mother }}</td>
        </tr>

          <tr>
            <th>Town:</th>
            <td>{{ $owner->town }}</td>
        </tr>
        <tr>
          <th>Laanta:</th>
          <td>{{ $owner->line }}</td>
      </tr>
      <tr>
        <th>Zone:</th>
        <td>{{ $owner->zone }}</td>
    </tr>
    <tr>
      <th>Phone:</th>
      <td>{{ $owner->phone }}</td>
  </tr>
                <tr>
                  <th>Date Created: </th>
                  <td>{{ $owner->created_at->format('m/d/Y') }}</td>
              </tr>
              <tr>
                <th>Date Updated: </th>
                <td>{{ $owner->updated_at->format('m/d/Y') }}</td>
            </tr>

          </h4>
        </table>


            </div>

          </div>
          <br>
        </div>
      </div>
      </div>
    </div>
<!-- new div row 2-->


                        <!-- second column -->

                        <div class="row">

                        <div class="col-lg-12">
                            <div class="card m-t-35">
                                <div class="card-header bg-info">
                                    <div>
                                        <i class="fa fa-pencil"></i>
                                        Owner's Land
                                    </div>
                                </div>
                                <div class="card-block m-t-35 padding">
                                    <div class="feed">
                                      Number of Land(s)<span class="badge badge-pill badge-primary float-right">{{count($owner->lands)}}</span>

                                      <table class="table table-responsiv">
                                        <tr>
                                          <td>Town</td>
                                        <td>Latitude</td>
                                        <td>Longitude</td>
                                        <td>laanta</td>
                                        <td>Area of land</td>
                                        <td class="action">Action</td>
                                      </tr>

                                      @foreach($owner->lands as $land)
                                      <tr>

                                        <td>{{$land->town}}</td>
                                        <td>{{$land->latitude}}</td>
                                        <td>{{$land->longitude}}</td>
                                        <td>{{$land->laanta}}</td>
                                        <td>{{$land->areaofland}}</td>

                                        <td class="no-sort no-click" id="bread-actions">

                                          <a href="{{route('lands.show', [$land->id])}}" title="View" class="btn btn-sm btn-warning pull-right">
                                          <i class="fa fa-eye"></i> <span class="hidden-xs hidden-sm">View   </span>
                                          </a>
                                          <a href="{{route('lands.edit', [$land->id])}}" title="Edit" class="btn btn-sm btn-warning pull-right">
                                          <i class="fa fa-eye"></i> <span class="hidden-xs hidden-sm">Edit   </span>
                                          </a>


                                        </td>

                                      </tr>

                                      @endforeach
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>

              </div>
          </div>
      </div>
        <!-- /.outer -->
    </div>
    <!-- /.outer -->
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<!--plugin scripts-->
  <script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/pluginjs/dataTables.tableTools.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.rowReorder.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.colVis.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.html5.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.scroller.min.js')}}"></script>
  <!-- end of plugin scripts -->
  <!--Page level scripts-->
  <script type="text/javascript" src="{{asset('assets/js/pages/datatable.js')}}"></script>
    <!-- end page level scripts -->
@stop
