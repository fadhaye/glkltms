@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Dashboard
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/c3/css/c3.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/switchery/css/switchery.min.css')}}" />

@stop


{{-- Page content --}}
@section('content')
    <header class="head">
        <div class="main-bar">
            <div class="row">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-home"></i>
                        Dashboard
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
          <div class="row">
                                  <div class="col-lg-12">
                                      <div class="card">
                                          <div class="card-header bg-white">
                                              Register New Land Owner
                                          </div>
                                          <div class="card-block">
                                            <div class="row">
                                              <form method="POST" action="{{route('owners.update', [$owner->id])}}">
                                                {{ csrf_field() }}
                                                {{method_field('PUT')}}
                                              <div class="form-group row m-t-25">
                                   <div class="col-lg-3 text-center text-lg-right">
                                       <label class="col-form-label">Profile Pic</label>
                                   </div>
                                   <div class="col-lg-6 text-center text-lg-left">
                                       <div class="fileinput fileinput-new" data-provides="fileinput">
                                           <div class="fileinput-new img-thumbnail text-center">
                                               <img src="/../{{$owner->photo}}" width="231" height="171"  alt="not found"></div>
                                           <div class="fileinput-preview fileinput-exists img-thumbnail"></div>
                                           <div class="m-t-20 text-center">
                                             <span class="btn btn-primary btn-file">
                                                  <span class="fileinput-new">Select image</span>
                                                  <span class="fileinput-exists">Change</span>
                                                  <input type="file" value="{{$owner->photo}}"  name="photo" ></span>
                                                </span>
                                               <a href="#" class="btn btn-warning fileinput-exists"
                                                  data-dismiss="fileinput">Remove</a>
                                           </div>

                                       </div>
                                   </div>
                               </div>
                                            </div>
                                <div class="row">

                                                  <div class="col-lg-4 input_field_sections">
                                                      <h5>First</h5>
                                                        <input class="form-control" placeholder="First Name" value="{{ $owner->fname }}" required="" name="fname" type="text" id="fname">
                                                  </div>
                                                  <div class="col-lg-4 input_field_sections">
                                                      <h5>Second Name</h5>
                                                        <input class="form-control" placeholder="" value="{{ $owner->sname }}" required="" name="sname" type="text" id="fname">

                                                  </div>
                                                  <div class="col-lg-4 input_field_sections">
                                                      <h5>Third Name</h5>
                                                        <input class="form-control" placeholder=""value="{{ $owner->tname }}" required="" name="tname" type="text" id="fname">
                                                  </div>
                                    </div>
                                    <div class="row">
                                                  <div class="col-lg-4 input_field_sections">
                                                      <h5>Puntland ID</h5>
                                                        <input class="form-control" placeholder="989876"value="{{ $owner->pid }}" required="" name="pid" type="number" id="pid">
                                                  </div>
                                                  <div class="col-lg-4 input_field_sections">
                                                      <h5>Adress</h5>
                                                        <input class="form-control" placeholder="address" value="{{ $owner->address }}" required="" name="address" type="text" id="address">
                                                  </div>
                                                  <div class="col-lg-4 input_field_sections">
                                                      <h5>Date of Birth</h5>
                                                        <input class="form-control" placeholder="11/11/1111" value="{{ $owner->dob }}"  required="" name="dob" type="date" id="dob">
                                                  </div>
                                        </div>
                                        <div class="row">
                                                <div class="col-lg-4 input_field_sections">
                                                    <h5>Gender</h5>

                                                    <input type="radio"  name="gender" value="male" {{ $owner->gender == 'male' ? 'checked' : ''}}>  Male<br>
                                                    <input type="radio" name="gender" value="female" {{ $owner->gender == 'female' ? 'checked' : ''}}>Female<br>
                                                  </div>
                                                <div class="col-lg-4 input_field_sections">
                                                    <h5>Phone Number</h5>
                                                    <input class="form-control" placeholder="090-6666666" value="{{ $owner->phone }}" required="" name="phone" type="number" id="phone">
                                                </div>
                                                <div class="col-lg-4 input_field_sections">
                                                    <h5>Email</h5>
                                                    <input class="form-control" placeholder="abc@abc.com" value="{{ $owner->email }}"  name="email" type="email" id="email">
                                                </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-lg-9 push-lg-3">
                                              <input class="btn btn-primary offset-10" type="submit" value="Update">
                                              </form>

                                            </div>


                                          </div>

                                          </div>
                                      </div>
                                  </div>
                              </div>

            <!-- /.inner -->
        </div>
        <!-- /.outer -->
    </div>
    <!-- /.outer -->
@stop
{{-- page level scripts --}}
@section('footer_scripts')
    <!--  plugin scripts -->
    <script type="text/javascript" src="{{asset('assets/js/pluginjs/jasny-bootstrap.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/holderjs/js/holder.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/js/pages/validation.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/raphael/js/raphael-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/d3/js/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/c3/js/c3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/toastr/js/toastr.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/switchery/js/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.js')}}" ></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.resize.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.stack.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.time.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotspline/js/jquery.flot.spline.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.categories.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.pie.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jquery_newsTicker/js/newsTicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/countUp.js/js/countUp.min.js')}}"></script>
    <!--end of plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/new_dashboard.js')}}"></script>
    <!-- end page level scripts -->
@stop
