<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="UTF-8">
    <title>
        @section('title')
            | Galkacy LTMS
        @show
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('assets/img/logo.ico')}}"/>
    <!-- global styles-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />

    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/components.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/custom.css')}}"/>
    <!-- end of global styles-->

    @yield('header_styles')
</head>
<!-- tets -->

<body  class="fixed_menu">

<div class="bg-dark" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <nav class="navbar navbar-static-top" style="background-color: #00a230;">

            <div class="container-fluid m-0">
                <a class="navbar-brand float-left text-center" href="{{route('welcome')}}">
                    <h4 class="text-white"><img src="{{asset('assets/img/logo.png')}}" class="admin_img" alt="logo"> Galkacyo LTMS</h4>
                </a>

                <div class="menu">
                    <span class="toggle-left" id="menu-toggle">
                        <i class="fa fa-bars text-white">  &nbsp;Land Tenure Database Management System</i>
                  </span>

                </div>
                <!-- search scrip jquery js is up  -->

                <div class="topnav dropdown-menu-right float-right">

                    <div class="btn-group">
                      <form class="form-inline my-2 my-lg-0">
                        <a href=""></a>
                        <input type="search" id="search"  placeholder="Search" class="form-control" >
                        <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
                      </form>
                        <!-- <a class="btn btn-default btn-sm messages toggle-right">
                            &nbsp;
                            <i class="fa fa-cog text-white"></i>
                            &nbsp;
                        </a> -->

                    </div>

                    <div class="btn-group">
                        <div class="user-settings no-bg">
                            <button type="button" class="btn btn-default no-bg micheal_btn" data-toggle="dropdown">
                                <img src="/storage/{{Auth::user()->photo}}" class="admin_img2 rounded-circle avatar-img" alt="avatar">
                                <strong>{{ str_limit(Auth::user()->name, 10, '' ) }}</strong>
                                <span class="fa fa-sort-down white_bg"></span>
                            </button>
                            <div class="dropdown-menu admire_admin">
                                <a class="dropdown-item title" href="{{route('auth.show', [Auth::user()->id])}}">
                                    Profile</a>

                                    <a class="dropdown-item" href="{{route('auth.show', [Auth::user()->id])}}">
                                      <i class="fa fa-user"></i>
                                        Profile
                                      </a>
                                      <a class="dropdown-item" href="{{route('changepassword', [Auth::user()->id])}}">
                                        <i class="fa fa-key"></i>
                                          Chenge Password
                                        </a>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
                                        Logout
                                    </a>


                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>


            <!-- /.container-fluid -->
          </nav>
        <!-- /.navbar -->
        <!-- /.head --> </div>
    <!-- /#top -->
    <!-- left side bar -->
    <div class="wrapper">
        <div id="left" class="fixed" style="background-color: #4189dd;">
            <div class="menu_sction menu_scroll">
                <div class="media user-media bg-dark dker">
                    <div class="user-media-toggleHover">
                        <span class="fa fa-user"></span>
                    </div>
                    <div class="user-wrapper bg-dark">
                  <a class="user-link" href="">
                    <img class="media-object img-thumbnail user-img rounded-circle admin_img3" alt="User Picture" src="/storage/{{Auth::user()->photo}}">
                    <p class="text-white user-info">Welcome {{ str_limit(Auth::user()->name, 10, '' ) }}</p>
                  </a>

                <div class="search_bar col-lg-12">
                    <div class="input-group">
                        <!-- <input type="text" id="search" class="form-control" placeholder="search"> -->
                        <!-- <span class="input-group-btn">
                                <button class="btn without_border" type="button"><span class="fa fa-search" >
                                </span></button>
                            </span> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- #menu -->
        <ul id="menu" class="bg-blue dker">

            <li {!! (Request::is('welcome')? 'class="active"':"") !!}>
                <a href="{{route('welcome')}}">
                    <i class="fa fa-home"></i>
                    <span class="link-title">&nbsp;Dashboard</span>
                </a>
            </li>
            @role('superadministrator|admin|owners')

            <li {!! (Request::is('owners')|| Request::is('owner.create') ? 'class="active"':"") !!}>
              <a href="{{route('owners.index')}}">
                <i class="fa fa-users"></i>
                <span>Owners</span></a>
                <ul>
                  <li {!! (Request::is('owner.create') ? 'class="active"' : '') !!}>
                    <a href="{{route('owners.index')}}">
                      <i class="fa fa-users"></i>
                      <span>&nbsp; Owners</span></a>
                  </li>
                  <li {!! (Request::is('owner.create') ? 'class="active"' : '') !!}>
                      <a href="{{route('owners.create')}}">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                          <span>&nbsp; Create New Owner</span>
                      </a>
                  </li>
                </ul>
              </li>
              @endrole
              @role('superadministrator|admin|lands')

              <li {!! (Request::is('lands') ? 'class="active"' : '') !!}>
                <a href="{{route('lands.index')}}">
                <i class="fa fa-map-marker ">
                </i> <span>Land</span>
                </a>
                <ul>
                  <li><a href="{{route('lands.index')}}">
                  <i class="fa fa-map-marker ">
                  </i> <span>All Land</span>
                  </a></li>
                  <li><a href="{{route('lands.createland')}}">
                  <i class="fa fa-plus ">
                  </i> <span>Create New Land</span>
                  </a>
                </li>
                <li><a href="{{route('lands.transfer')}}">
                <i class="fa fa-share ">
                </i> <span>Lands Transfer</span>
                </a></li>
                <li><a href="{{route('lands.maps')}}">
                <i class="fa fa-map-marker ">
                </i> <span>Lands Map</span>
                </a></li>
                </ul>
              </li>
              @endrole

              @role('superadministrator|admin')
                <li {!! (Request::is('users')|| Request::is('user.index') ? 'class="active"':"") !!}>
                <a href="{{route('owners.index')}}">
                  <i class="fa fa-lock"></i>
                  <span>User Management</span></a>
                  <ul>
                    <li {!! (Request::is('auth.index') ? 'class="active"' : '') !!}>
                      <a href="{{route('auth.index')}}">
                        <i class="fa fa-user">||</i><i class="fa fa-lock"></i>
                        <span>&nbsp; Users </span></a>
                    </li>
                    <li {!! (Request::is('register') ? 'class="active"' : '') !!}>
                        <a href="{{route('auth.create')}}">
                          <i class="fa fa-plus" aria-hidden="true"></i>
                            <span>&nbsp; Create New User</span>
                        </a>
                    </li>
                    @role('superadministrator')
                    <li {!! (Request::is('log.index') ? 'class="active"' : '') !!}>
                        <a href="{{route('activitylog')}}">
                          <i class="fa fa-user-secret"></i>
                          <span>&nbsp; System Logs</span>
                        </a>
                    </li>
                    @endrole

                  </ul>
                </li>
                @endrole



              </ul>


                </div>
        <!-- /#menu -->
    </div>
    <!-- /#left -->
    <div id="content" class="bg-container">
        <!-- Content -->

        @yield('content')


        <!-- Content end -->
    </div>


</div>
@include('layouts.right_sidebar')
<!-- # right side -->
</div>
<!-- global scripts-->
<!-- the first one is jquery -->
<script type="text/javascript" src="{{asset('assets/js/components.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script>
<script>

  $( function() {

    $( "#search" ).autocomplete({

      source: '{{route('search')}}',
      select: function(event, ui) {
            var url = "";
            if(url != '#') {
                location.href = '/owners/' + url;
            }},
    });
  } );
  </script>
<!-- end of global scripts-->
<!-- page level js -->
@yield('footer_scripts')
<!-- end page level js -->
</body>
</html>
