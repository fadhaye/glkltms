@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Dashboard
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}"/>
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>

   <!--End of plugin styles-->
   <!--Page level styles-->
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/>
   <!-- end of page level styles -->
   <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/c3/css/c3.min.css')}}"/>
   <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/toastr/css/toastr.min.css')}}"/>
   <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/switchery/css/switchery.min.css')}}" />
   <!--page level styles-->
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/new_dashboard.css')}}"/>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
   <!-- <script src="https://chartjs-plugin-datalabels.netlify.com/chartjs-plugin-datalabels.js"></script> -->

@stop


{{-- Page content --}}
@section('content')
    <header class="head">
        <div class="main-bar">
            <div class="row">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-home"></i>
                        Dashboard
                    </h4>
                </div>
            </div>
        </div>

    </header>



<div class="outer">
<div class="inner bg-container">
    <div class="row">
        <div class="col-xl-12 col-lg-7 col-xs-12">

    <div class="row">

                <div class="col-sm-6 col-xs-12">
                    <div class="bg-primary top_cards">
                        <div class="row icon_margin_left">

                            <div class="col-lg-5 icon_padd_left">
                                <div class="float-xs-left">
                                      <span class="fa-stack fa-sm">
                                      <i class="fa fa-circle fa-stack-2x"></i>
                                      <i class="fa fa fa-map-marker  fa-stack-1x fa-inverse text-primary sales_hover"></i>
                                      </span>
                                  </div>
                              </div>
                            <div class="col-lg-7 icon_padd_right">
                                <div class="float-right cards_content">
                                    <span class="number_val" id="sales_count">{{count($lands)}}</span>
                                    <i class="fa fa-long-arrow-up fa-2x"></i>
                                    <br/>
                                    <span class="card_description">Land</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              <div class="col-sm-6 col-xs-12">
                  <div class="bg-success top_cards">
                      <div class="row icon_margin_left">
                          <div class="col-lg-5 icon_padd_left">
                              <div class="float-xs-left">
                          <span class="fa-stack fa-sm">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <i class="fa fa-users fa-stack-1x fa-inverse text-success visit_icon"></i>
                          </span>
                                                                  </div>
                          </div>
                          <div class="col-lg-7 icon_padd_right">
                              <div class="float-right cards_content">
                                  <span class="number_val" id="sales_count">{{count($owners)}}</span>
                                  <i class="fa fa-long-arrow-up fa-2x"></i>
                                  <br/>
                                  <span class="card_description">Owners</span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-sm-6 col-xs-12">
                  <div class="bg-warning top_cards">
                      <div class="row icon_margin_left">

                          <div class="col-lg-5 icon_padd_left">
                              <div class="float-xs-left">
                                    <span class="fa-stack fa-sm">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa fa-male  fa-stack-1x fa-inverse text-primary sales_hover"></i>
                                    </span>
                                </div>
                            </div>
                          <div class="col-lg-7 icon_padd_right">
                              <div class="float-right cards_content">
                                  <span class="number_val" id="sales_count">{{($countmale)}}</span>
                                  <i class="fa fa-long-arrow-up fa-2x"></i>
                                  <br/>
                                  <span class="card_description">Male</span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-sm-6 col-xs-12">
                  <div class="bg-mint top_cards">
                      <div class="row icon_margin_left">

                          <div class="col-lg-5 icon_padd_left">
                              <div class="float-xs-left">
                                    <span class="fa-stack fa-sm">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa fa-female  fa-stack-1x fa-inverse text-primary sales_hover"></i>
                                    </span>
                                </div>
                            </div>
                          <div class="col-lg-7 icon_padd_right">
                              <div class="float-right cards_content">
                                  <span class="number_val" id="sales_count">{{($countfemale)}}</span>
                                  <i class="fa fa-long-arrow-up fa-2x"></i>
                                  <br/>
                                  <span class="card_description">Female</span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

                                  </div>
                          </div>
                        </div><br>
<div class="row">
  <div class="col-md-4">
     <canvas id="myChart" width="400" height="300"></canvas>
  </div>
  <div class="col-md-4">
    <canvas id="chartjs-4" class="chartjs" width="400" height="300"></canvas>
      </div>
      
  <div class="col-md-4">
     <canvas id="myCharts" width="400" height="300"></canvas>
  </div>

</div>


  </div>




@stop
{{-- page level scripts --}}
@section('footer_scripts')
<!--Plugin scripts-->
<script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Israac", "Garsoor", "Barwaaqo"],
        datasets: [{
            label: '# of Towns',
            dataLabels: {
              colors: ['#fff', '#ccc', '#000'],
              minRadius: 30,
            },
            data: [ {{($countisraac)}},{{($countgarsoor)}},{{($countbarwaaqo)}} ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});




var ctxs = document.getElementById("myCharts");
var myChart = new Chart(ctxs, {
    type: 'bar',
    data: {
        labels: ["Male", "Female"],
        datasets: [{
            label: "Gender",
            data: [ {{($countmale)}},{{($countfemale)}}],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)'

            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'

            ],
            borderWidth: 1,
            data: [ {{($countmale)}},{{($countfemale)}}],
            dataLabels: {
              colors: [

                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ]
        }
        }]
    },
    options: {

        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

new Chart(document.getElementById("chartjs-4"),
{ type:"doughnut",
    data:
      {
        labels:["Beer","Ganacsi","Deegaan"],
        datasets:[{
              label:"My First Dataset",
              data:[
                {{($countBeerid)}},{{($countGanacsi)}},
                {{($countDeedaan)}}
              ],
            backgroundColor:[
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)'
            ]
            }
          ]},

        });


</script>


   <script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.colVis.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.html5.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>
   <!--End of plugin scripts-->
   <!--Page level scripts-->
   <script type="text/javascript" src="{{asset('assets/js/pages/users.js')}}"></script>
   <!-- end page level scripts -->
@stop
