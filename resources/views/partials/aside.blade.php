<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
  <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
    </div>
  </form>
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">HEADER</li>
    <!-- Optionally, you can add icons to the links -->
    <li class="active"><a href="{{route('owners.index')}}"><i class="fa fa-users"></i> <span>Owners</span></a></li>
    <li><a href="{{route('lands.index')}}"><i class="fa fa-map-marker "></i> <span>Land</span></a></li>
    <li class="treeview">
      <a href="#"><i class="fa fa-lock"></i> <span>User Management</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-user"></i>Users</a></li>
        <li><a href="#"><i class="fa fa-user-secret"></i>Roles</a></li>
        <li><a href="#"></a></li>
      </ul>
    </li>
  </ul>
  <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
