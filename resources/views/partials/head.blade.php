<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>GalkacyoLTMS</title>

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/ionicons.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/skins/skin-green.css') }}" rel="stylesheet">
<script src="{{ asset('js/vue.min.js') }}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/buttons.css')}}"/>
 <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/Buttons/css/buttons.min.css')}}"/>

@yield('style')
