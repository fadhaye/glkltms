@extends('layouts/default')
{{-- Page title --}}
@section('title')
    Dashboard
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}"/>
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>
   <!--End of plugin styles-->
   <!--Page level styles-->
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/>
   <!-- end of page level styles -->

@stop


{{-- Page content --}}
@section('content')
<header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-lg-6 col-sm-4">
                    <h4 class="nav_top_align">
                        <i class="fa fa-eye"></i>
                        System Log
                    </h4>
                </div>
                <div class="col-lg-6 col-sm-8 col-12">

                </div>
            </div>
        </div>
    </header>
    <div class="outer">
      @if (session('status'))
      <div class="alert alert-info">
        <a class="close" data-dismiss="alert">×</a>
              {{ session('status') }}
            </div>
      @endif
      @if(Session::has('msg'))
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Well Done!</strong> {!!Session::get('msg')!!}
        </div>
        @endif

    <div class="inner bg-container">
        <div class="card">
            <div class="card-header bg-warning">
              <i class="fa fa-lock"></i>
            </div>
            <div class="card-block m-t-35" id="user_body">
                <div class="table-toolbar">
                    <div class="btn-group">
                    </div>
                    <div class="btn-group float-right users_grid_tools">
                        <div class="tools"></div>
                    </div>
                </div>
                <div>
                    <div>
                        <table class="table  table-striped table-bordered table-hover "
                               id="editable_table" role="grid" data-order='[[ 0, "desc" ]]'>
                            <thead>
                            <tr role="row">
                                <th>Time</th>
                                <th>Description</th>
                                <th>User</th>
                            </tr>
                            </thead>
                            <tbody>
                              @foreach($logItems as $logItem)
                            <tr>
                                <td>{{ $logItem->created_at->format('d/m/Y') }}
                                  <!-- <small>({{ $logItem->created_at->diffForHumans() }})</small> -->
                                </td>
                                <td>
                                  {{$logItem->description }}
                                  <!-- ||

                                  {{$array=$logItem->changes() }}<br> -->



                                  @if($logItem->subject)
                                  {{ $logItem->subject->name }}<br>
                                  {{ $logItem->subject->owner_id }}<br>
                                  {{ $logItem->subject->witness1 }}<br>
                                  {{ $logItem->subject->witness2 }}<br>
                                  {{ $logItem->subject->witness3 }}<br>
                                  @endif

                                   </td>
                                <td>
                                    @if($logItem->causer)
                                        <a href="{{ route('auth.show', [$logItem->causer->id]) }}">
                                            {{ $logItem->causer->name }}
                                        </a>
                                    @endif
                                </td>

                            </tr>
                            @endforeach



                            </tbody>
                        </table>
                    </div>
                </div>

@stop
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('assets/vendors/datatables/js/dataTables.responsive.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.colVis.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>
<!--End of plugin scripts-->
<!--Page level scripts-->
<script type="text/javascript" src="{{asset('assets/js/pages/users.js')}}"></script>
@stop
