@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Dashboard
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/inputlimiter/css/jquery.inputlimiter.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/chosen/css/chosen.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jquery-tagsinput/css/jquery.tagsinput.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/daterangepicker/css/daterangepicker.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datepicker/css/bootstrap-datepicker.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/fileinput/css/fileinput.min.css')}}"/>
    <!--End of plugin styles-->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/form_elements.css')}}"/>
    <!-- end of page level styles -->
@stop
@section('content')

    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-home"></i>
                        Dashboard
                    </h4>
                </div>
            </div>
        </div>
    </header>

  <div class="outer">
    <div class="row">
      <div class="col-12">
        <div class="card-header bg-BLUE">
            Update or Transfer this land
          </div>
        <div class="card-block m-t-35">

        </div>
      </div>

    </div>
      <div class="inner bg-container">


        <div class="row">

            <div class="col-lg-12">
              <form method="POST" action="{{ route('lands.update', [$land->id]) }}">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div class="row">
                  <div class="col-lg-12 input_field_sections">
                    <h5>Select Owner or search</h5>
                    <select name="owner_id" class="form-control chzn-select" tabindex="2">
                      <option disabled selected>Choose a new Owner</option>
                      <option selected value="{{$land->owner->id}}">{{$land->owner->fname}}</option>
                        @foreach($transfers as $transfer)
                        <option value="{{$transfer->id}}">{{$transfer->fname}} {{$transfer->sname}} [{{$transfer->pid}}]</option>
                        @endforeach
                    </select>
                    <!--</div>-->

                </div>

              </div>


              <div class="row">
                <div class="col-sm">
                  <h5>Latitude</h5>
                  <div class="form-group">
                    <input class="form-control" type="text"  value="{{$land->latitude}}" required id="lat"  name="latitude" value="">
                  </div>

                </div>
                  <div class="col-sm">
                  <h5>Longitude</h5>
                  <div class="form-group">
                    <input class="form-control" value="{{$land->longitude}}" required type="text" id="lng"  name="longitude" value="">
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-sm">
                  <h5>Land Type</h5>
                  <div class="form-group">
                      <select required="" name="type" required class="form-control">
                        <option  value="{{$land->type}}">{{$land->type}}</option>
                          <option value="Deegaan">Deegaan</option>
                          <option value="Ganacsi">Ganacsi</option>
                          <option value="Beerid">Beerid</option>
                        </select>
                  </div>

                </div>
                  <div class="col-sm">
                    <h5>Town</h5>
                    <div class="form-group">
                        <select required name="town" class="form-control">
                          <option  value="{{$land->town}}">{{$land->town}}</option>
                            <option value="Israac">Israac</option>
                            <option value="Garsoor">Garsoor</option>
                            <option value="Barwaaqo">barwaaqo</option>
                          </select>
                    </div>
                </div>
              </div>



              <div class="row">
                <div class="col-sm">
                  <h5>Line</h5>
                  <div class="form-group">
                      <select required="" name="line" class="form-control">
                        <option  value="{{$land->laanta}}">{{$land->laanta}}</option>

                           <option value="Laanta 1">Laanta 1</option>
                          <option value="Laanta 2">Laanta 2</option>
                          <option value="Laanta 3">Laanta 3</option>
                          <option value="Laanta 4">Laanta 4</option>
                          <option value="Laanta 5">Laanta 5</option>
                          <option value="Laanta 6">Laanta 6</option>
                        </select>
                  </div>
                </div>
                <div class="col-sm">
                  <h5>Zone</h5>
                  <div class="form-group">
                    <input class="form-control" value="{{$land->zone}}" required type="text" name="zone" value="">
                  </div>
                </div>
              </div>

              <div class="row">
                 <div class="col-sm">
                   <h5>Width (Meter)</h5>
                   <div class="form-group">
                     <input class="form-control" value="{{$land->width}}" type="text" name="width" value="">
                   </div>
                 </div>
                 <div class="col-sm">
                   <h5>Length (Meter)</h5>
                   <div class="form-group">
                     <input class="form-control" value="{{$land->length}}" type="text" name="length" value="">
                   </div>
                 </div>
               </div>


               <div class="row">
                  <div class="col-sm">
                    <h5>Area</h5>
                    <div class="form-group">
                      <input class="form-control" value="{{$land->areaofland}}" type="text" name="area" value="">
                    </div>
                  </div>

                  <div class="col-sm">
                    <h5 class="invisible">test</h5>
                    <div class="form-group">
                      <input class="form-control" required type="text" name="north" value="" disabled>
                    </div>
                  </div>
                </div>
                <div class="row">
                   <div class="col-sm">
                     <h5>North</h5>
                     <div class="form-group">
                       <input class="form-control" value="{{$land->north}}" required type="text" name="north" value="">
                     </div>
                   </div>
                   <div class="col-sm">
                     <h5>West</h5>
                     <div class="form-group">
                       <input class="form-control" value="{{$land->west}}" required type="text" name="west" value="">
                     </div>
                   </div>

                 </div>

                 <div class="row">
                    <div class="col-sm">
                      <h5>East</h5>
                      <div class="form-group">
                        <input class="form-control" value="{{$land->east}}" required type="text" name="east" value="">
                      </div>
                    </div>
                    <div class="col-sm">
                      <h5>South</h5>
                      <div class="form-group">
                        <input class="form-control" value="{{$land->south}}" required type="text" name="south" value="">
                      </div>
                    </div>
                  </div>


                  <div class="row">
                     <div class="col-sm">
                       <h5>Witness one</h5>
                       <div class="form-group">
                         <input class="form-control"value="{{$land->witness1}}"  required type="text" name="witness1" value="">
                       </div>
                     </div>
                     <div class="col-sm">
                       <h5>Witness two</h5>
                       <div class="form-group">
                         <input class="form-control" value="{{$land->witness2}}" required type="text" name="witness2" value="">
                     </div>
                   </div>
                 </div>

                   <div class="row">

                      <div class="col-sm">
                        <h5>Witness three</h5>
                      <div class="form-group">
                        <input class="form-control" value="{{$land->witness3}}" type="text" required name="witness3" value="">
                    </div>
                  </div>
                  <div class="col-sm">
                    <h5 class="invisible">test</h5>
                      <div class="form-group">
                    <input type="file" class="form-control" name="Notery" value="">
                    </div>
                </div>
                  </div>
              </div>



                    <input class="btn btn-primary offset-10" type="submit" value="Transfer">


            </form>

        </div>
      </div>


    </div>
    <!-- end of class 12 -->
    </div>

@stop
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/jquery.uniform/js/jquery.uniform.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/inputlimiter/js/jquery.inputlimiter.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/chosen/js/chosen.jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jquery-tagsinput/js/jquery.tagsinput.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/validval/js/jquery.validVal.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/autosize/js/jquery.autosize.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/inputmask/js/inputmask.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/inputmask/js/jquery.inputmask.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/inputmask/js/inputmask.date.extensions.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/inputmask/js/inputmask.extensions.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/fileinput/js/fileinput.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/fileinput/js/theme.js')}}"></script>
    <!--end of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="{{asset('assets/js/form.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/form_elements.js')}}"></script>
    <!-- end page level scripts -->
@stop
