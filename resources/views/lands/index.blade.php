@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Dashboard
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}"/>
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>
   <!--End of plugin styles-->
   <!--Page level styles-->
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/>
   <!-- end of page level styles -->
@stop
@section('content')

    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-home"></i>
                        Dashboard
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
          <div class="row">
            <div class="col-12">
              @section('maptitle', 'Overview of registred Lands')

            </div>

          </div>
            <div class="card">
                <div class="card-header bg-warning">
                     Land List
                </div>
                <div class="card-block m-t-35" id="user_body">
                    <div class="table-toolbar">
                        <div class="btn-group">

                            </a>
                        </div>
                        <div class="btn-group float-xs-right users_grid_tools">
                            <div class="tools"></div>
                        </div>
                    </div>
                    <div>
                        <div>
                              <table class="table  table-striped table-bordered table-hover dataTable no-footer"
                                       id="editable_table" data-order='[[ 8, "dec" ]]' role="grid">
                                  <thead>
                              <tr>
                                <th>Owner ID</th>
                                <th>Type</th>
                                <th>town</th>
                                <th>laanta</th>
                                <th>Zone</th>
                                <th>width</th>
                                <th>length</th>
                                <th>Area of Land</th>
                                <th>Created</th>
                                <th class="actions">Actions</th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach ($lands as $land)
                                <tr>
                                  <td><a href="{{route('owners.show', [$land->owner->id])}}">{{$land->owner->pid}}</a></td>

                                  <td>{{$land->type}}</td>
                                  <td>{{$land->town}}</td>
                                  <td>{{$land->laanta}}</td>
                                  <td>{{$land->zone}}</td>
                                  <td>{{$land->width}}</td>
                                  <td>{{$land->length}}</td>
                                  <td>{{$land->areaofland}}</td>
                                  <td>{{ $land->created_at->format('d/m/Y') }}</td>

                                  <td class="no-sort no-click" id="bread-actions">


                                  <a href="{{route('lands.show', [$land->id])}}" title="View" class="btn btn-sm btn-warning pull-right">
                                  <i class="fa fa-eye"></i> <span class="hidden-xs hidden-sm">View   </span>
                                  </a>
                                  </td>
                                </tr>
            @endforeach
          </tbody>

</table>
</div>

<!-- <a href="{{route('lands.edit', [$land->id])}}" title="Transfer" class="btn btn-sm btn-primary pull-right edit">
<i class="fa fa-edit"></i> <span class="hidden-xs hidden-sm">Transfer</span>
</a> -->

        </div>
    </div>
@stop
@section('footer_scripts')

<!--Plugin scripts-->
   <script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.colVis.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.html5.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>

<script type="text/javascript">


</script>
   <!--End of plugin scripts-->
   <!--Page level scripts-->
   <script type="text/javascript" src="{{asset('assets/js/pages/users.js')}}"></script>
   <!-- end page level scripts -->

@stop
