@extends('layouts/default')
{{-- page level styles --}}
@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/chartist/css/chartist.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/circliful/css/jquery.circliful.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/invoice.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}"/>

<style type="text/css">
  
  .img{
      width: 120px;
      height: 120px;
    }
</style>
@stop
@section('content')
      <div class="inner bg-container">
      </div>
                    <div class="col-lg-6 col-sm-6 invoice_print" >
                              <span class="pull-sm-right">
                                  <a style="color:#fff;" class="btn button-alignment btn-info m-t-15" data-toggle="button" onclick="javascript:window.print();">
                                      Print
                                  </a>
                              </span>
                    </div>
                    <br>
        <div class="inner bg-container" id="example">
        <div align="center" class="row">
              <div class="col-4">
                <br>
                <h3>DOWLADDA <br>PUNTLAND EE <br>SOOMAALIYA</h3>
              </div>
              <div class="col-4">
                
                <img class="img" src="{{asset('assets/img/logo.png')}}">
              </div>
              <div class="col-4">
                                <br>

                <h3>PUNTLAND <br>GOVERNMENT OF <br>SOMALIA</h3>
              </div>
        </div>
        <div class="row">
          <div class="col-2">
          </div>
            <div class="col-8">
              <h4 align="center" >DOWLADDA HOOSE EE DEGMADA GAALKACYO</h4><br>
            <div class="col-2">
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-2">
        </div>

          <div align="center" class="col-8">
            <h4>Shahaadada Mulkiyadda Dhulka (Sobro Loogo)</h4><br>
          <div class="col-2">
          </div>
      </div>
    </div>
<div class="row">
    <div class="col-4">
      <h4>Taariikh: {{ $land->updated_at->format('m/d/Y') }} </h4>
    </div>

      <div align="center" class="col-4">
      </div>

      <div align="right" class="col-4">
        <h4>Tix-Raac:<b>{{$land->id}}</b></h4>
      </div>
</div>
<table class="table table-dark">
  <thead>
    <tr>
      <th>Mr/Mrs: </th>
    <th>Puntland ID:</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    <td>Mr/Mrs: {{$land->owner->fname}} {{$land->owner->sname}} {{$land->owner->tname}}</td>
    <td>Puntland ID:{{$land->owner->pid}}</td>
  </tr>
  </tbody>
  
</table>
<div align="center" class="row">
  

  <div  class="col-12">
    <h3>Mr/Mrs: {{$land->owner->fname}} {{$land->owner->sname}} {{$land->owner->tname}}</h3>
    <h3>Puntland ID:{{$land->owner->pid}}</h3>
  </div>
</div>
  <br>
  <div align="center" class="row">

  <div  class="col-12">
    <h4>Waxaa Luguu Ogalaaday Dhulkaa soo Codsatay oo Cabirkiisu dhanyahay</h4>
    width*length<h2>{{$land->width}}*{{$land->length}}</h2>
    <h3>{{$land->areaofland}} M<sup>2</sup></h3>
  </div>
  <div  class="col-12">
  </div>
</div>
<br>
<div align="center" class="row">
  <div  class="col-12">
  </div>
</div>
<div align="center" class="row">
  <div  class="col-12">
    <h4>Ku Yaal Dagmada: <b>Gaalkacyo</b></h4>
    <h4>Tuulada ama Xaafadda: {{$land->town}},  {{$land->laanta}}, {{$land->zone}}</h4>
  </div>
</div>
<div align="center" class="row">
  <div class="col-12">
      <h2>SOOHDIN LA LEH</h2>
  </div>
</div>

<div align="center" class="row">
    <div class="col-6">
        Bari:{{$land->east}}
    </div>
    <div class="col-6">
        Waqooyi:{{$land->north}}
    </div>
    <div class="col-6">
      Koonfur: {{$land->south}}
    </div>
    <div class="col-6">
      Galbeed: {{$land->west}}
  </div>
</div>
<div align="center" class="row">
  <div class="col-12">
<h5>Dhulkaan Hadaadan Dhisan muddo Lix Bilood ah ma sii lahaan kartid</h5>
  </div>

</div>
<br>
<div align="center" class="row">

<div class="col-6">
Saxiixa Duqa Magaaladda
</div>
<div class="col-6">
Saxiixa Xoghayaha D/Hoose
</div>

</div>

    <!-- end of class 12 -->
  </div>



@stop

@section('footer_scripts')

</script>
    <script type="text/javascript" src="{{asset('assets/vendors/countUp.js/js/countUp.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/chartist/js/chartist.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/swiper/js/swiper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/circliful/js/jquery.circliful.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.js')}}" ></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.resize.js')}}"></script>
    <!--end of plugin scripts-->

@stop
