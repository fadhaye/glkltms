<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/maps.css')}}"/>

    <style media="screen">

    </style>
  </head>
  <body>


    <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card m-t-35">
                            <div class="card-header bg-warning">
                                @yield('maptitle')
                            </div>
                            <div class="card-block m-t-35">

                                <br />
                                <div id="map" class="gmap"></div>
                            </div>
                        </div>
                    </div>
                </div>




    <script>
    // The following example creates complex markers to indicate beaches near
// Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
// to the base of the flagpole.

function initMap() {
var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 13,
  center: {lat: 6.787436, lng: 47.441410},
  mapTypeId: 'satellite'

});
map.setTilt(45);
setMarkers(map);
}


// Data for the markers consisting of a name, a LatLng and a zIndex for the
// order in which these markers should display on top of each other.
var landes = [

@foreach ($lands as $land)

[{{ $land->id }}, {{ $land->latitude }}, {{ $land->longitude}}],
@endforeach

];

function setMarkers(map) {

var image = {
  icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
  // This marker is 20 pixels wide by 32 pixels high.
  size: new google.maps.Size(20, 32),
  // The origin for this image is (0, 0).
  origin: new google.maps.Point(0, 0),
  // The anchor for this image is the base of the flagpole at (0, 32).
  anchor: new google.maps.Point(0, 32)
};
// Shapes define the clickable region of the icon. The type defines an HTML
// <area> element 'poly' which traces out a polygon as a series of X,Y points.
// The final coordinate closes the poly by connecting to the first coordinate.
var shape = {
  coords: [1, 1, 1, 20, 18, 20, 18, 1],
  type: 'poly'
};
for (var i = 0; i < landes.length; i++) {
  var land = landes[i];
  var marker = new google.maps.Marker({
    position: {lat: land[1], lng: land[2]},
    map: map,
    icon: image,
    shape: shape,
    title: land[0],
    zIndex: land[3]
  });
}

}



    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGt7x_fDW-0NNnuIw6TBGuPWFPTWdh0-I&callback=initMap">
    </script>
  </body>
</html>
