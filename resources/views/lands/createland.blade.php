@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Dashboard
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/chartist/css/chartist.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/circliful/css/jquery.circliful.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/index.css')}}">

@stop

@section('content')

    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-home"></i>
                        Dashboard
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">

  <div class="outer">
    <div class="row">
      <div class="col-12">
        @include('lands.addmaps')

      </div>

    </div>
      <div class="inner bg-container">

<br><br>
<div class="card">
  <div class="card-header bg-white">
    Land Registration
  </div>
    <div class="card-block">
          <form method="POST" action="{{ route('lands.store') }}" accept-charset="UTF-8">
            {{ csrf_field() }}
            <br>
          <div class="row">
            <div class="col-sm">
              <h5>Latitude</h5>
              <div class="form-group">
                <input class="form-control" type="text" required id="lat"  name="latitude" value="">
              </div>

            </div>
              <div class="col-sm">
              <h5>Longitude</h5>
              <div class="form-group">
                <input class="form-control" required type="text" id="lng"  name="longitude" value="">
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-sm">
              <h5>Land Type</h5>
              <div class="form-group">
                  <select required="" name="type" required class="form-control">
                    <option value="">Select Project Land Type</option>
                      <option value="Deegaan">Deegaan</option>
                      <option value="Ganacsi">Ganacsi</option>
                      <option value="Beerid">Beerid</option>
                    </select>
              </div>

            </div>
              <div class="col-sm">
                <h5>Town</h5>
                <div class="form-group">
                    <select required name="town" class="form-control">
                      <option value="">Select Town</option>
                        <option value="Israac">Israac</option>
                        <option value="Garsoor">Garsoor</option>
                        <option value="Barwaaqo">barwaaqo</option>
                      </select>
                </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm">
              <h5>Line</h5>
              <div class="form-group">
                  <select required="" name="line" class="form-control">
                     <option value="">Select Line</option>
                      <option value="Laanta 1">Laanta 1</option>
                      <option value="Laanta 2">Laanta 2</option>
                      <option value="Laanta 3">Laanta 3</option>
                      <option value="Laanta 4">Laanta 4</option>
                      <option value="Laanta 5">Laanta 5</option>
                      <option value="Laanta 6">Laanta 6</option>
                    </select>
              </div>
            </div>
            <div class="col-sm">
              <h5>Zone</h5>
              <div class="form-group">
                <input class="form-control" required type="text" name="zone" value="">
              </div>
            </div>
          </div>

          <div class="row">
             <div class="col-sm">
               <h5>Width (Meter)</h5>
               <div class="form-group">
                 <input class="form-control" type="text" name="width" value="">
               </div>
             </div>
             <div class="col-sm">
               <h5>Length (Meter)</h5>
               <div class="form-group">
                 <input class="form-control" type="text" name="length" value="">
               </div>
             </div>
           </div>
           <div class="row">
              <div class="col-sm">
                <h5>Area</h5>
                <div class="form-group">
                  <input class="form-control" type="text" name="area" value="">
                </div>
              </div>

              <div class="col-sm">
                <h5 class="invisible">test</h5>
                <div class="form-group">
                  <input class="form-control" required type="text" name="north" value="" disabled>
                </div>
              </div>
            </div>


           <div class="row">
              <div class="col-sm">
                <h5>North</h5>
                <div class="form-group">
                  <input class="form-control" required type="text" name="north" value="">
                </div>
              </div>
              <div class="col-sm">
                <h5>West</h5>
                <div class="form-group">
                  <input class="form-control" required type="text" name="west" value="">
                </div>
              </div>

            </div>

            <div class="row">
               <div class="col-sm">
                 <h5>East</h5>
                 <div class="form-group">
                   <input class="form-control" required type="text" name="east" value="">
                 </div>
               </div>
               <div class="col-sm">
                 <h5>South</h5>
                 <div class="form-group">
                   <input class="form-control" required type="text" name="south" value="">
                 </div>
               </div>
             </div>

             <div class="row">
                <div class="col-sm">
                  <h5>Witness one</h5>
                  <div class="form-group">
                    <input class="form-control" required type="text" name="witness1" value="">
                  </div>
                </div>
                <div class="col-sm">
                  <h5>Witness two</h5>
                  <div class="form-group">
                    <input class="form-control" required type="text" name="witness2" value="">
                </div>
              </div>
            </div>

              <div class="row">

                 <div class="col-sm">
                   <h5>Witness three</h5>
                 <div class="form-group">
                   <input class="form-control" type="text" required name="witness3" value="">
               </div>
             </div>
             <div class="col-sm">
               <h5 class="invisible">test</h5>
               <div class="form-group">
                 <input class="form-control" required type="text" name="north" value="" disabled>
               </div>
             </div>
         </div>

              <!--end of row 4 -->
              <input class="btn btn-primary offset-10" type="submit" value="Save">

            </form>

        </div>
      </div>
    </div>


    </div>
    <!-- end of class 12 -->
    </div>
@stop
@section('footer_scripts')
    <!--  plugin scripts -->
    <script type="text/javascript" src="{{asset('assets/vendors/countUp.js/js/countUp.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flip/js/jquery.flip.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pluginjs/jquery.sparkline.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/chartist/js/chartist.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pluginjs/chartist-tooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/swiper/js/swiper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/circliful/js/jquery.circliful.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.js')}}" ></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.resize.js')}}"></script>
    <!--end of plugin scripts-->

    <script type="text/javascript" src="{{asset('assets/js/pages/index.js')}}"></script>
@stop
