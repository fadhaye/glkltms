<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>cool</title>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/maps.css')}}"/>

    <style media="screen">
    #map {
      /*height: 400px;
      width: 500px;*/
     }
    </style>
  </head>
  <body>


    <div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card m-t-35">
            <div class="card-header bg-warning">
                Galkacyo Map
            </div>
            <div class="card-block m-t-35">

                <br />
                <div id="map" class="gmap"></div>
            </div>
        </div>
    </div>

<script>

function initMap() {
var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 13,
  center: {lat: 6.787436, lng: 47.441410},
  mapTypeId: 'satellite'
});

var marker = new google.maps.Marker({
    position: {
      lat: 6.791367,
      lng: 47.458487
    },
    map:map,
    draggable:true
  });
    google.maps.event.addListener(marker, 'dragend', function(){
      var lat =marker.getPosition().lat();
      var lng =marker.getPosition().lng();
      $('#lat').val(lat);
      $('#lng').val(lng);
  });
}
    </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGt7x_fDW-0NNnuIw6TBGuPWFPTWdh0-I&callback=initMap">
    </script>
  </body>
</html>
