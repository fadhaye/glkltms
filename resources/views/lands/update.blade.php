@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Dashboard-2
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/chartist/css/chartist.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/circliful/css/jquery.circliful.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/index.css')}}">
@stop
@section('content')

    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-home"></i>
                        Dashboard
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
          <div class="row">
              <div class="col-6">
                  <h4 class="m-t-5">
                      <i class="fa fa-home"></i>
                      Dashboard
                  </h4>
              </div>
          </div>
      </div>
  </header>
  <div class="outer">
    <div class="row">
      <div class="col-12">
        @include('lands.addmaps')

      </div>

    </div>
      <div class="inner bg-container">


        <div class="row">

            <div class="col-lg-12">
              <form method="POST" action="{{ route('lands.store') }}" accept-charset="UTF-8"><input name="_token" type="hidden" value="ICPFxCa5fhWmIugl7aG2x0mSsYaFlSu0QAFW0D3N">
                {{ csrf_field() }}
                <input class="form-control" hidden="" type="number" name="owner_id" value="{{$owner->id}}">

                <div class="row">

                  <div class="col-lg-6 input_field_sections">
                   <h5>Latitude</h5>
                   <div class="form-group">
                     <input class="form-control" type="text" id="lat"  name="latitude" value="">
                   </div>
                 </div>
                 <div class="col-lg-6 input_field_sections">
                  <h5>Longitude</h5>
                  <div class="form-group">
                    <input class="form-control" type="text" id="lng"  name="longitude" value="">
                  </div>
                </div>

                </div>
              <!--First Row  -->
              <div class="row">
              <div class="col-lg-4 input_field_sections">

                 <h5>Land Type</h5>
                 <div class="form-group">
                     <select name="type" class="form-control">
                       <option value="0">Select Project Land Type</option>
                         <option value="1">Deegaan</option>
                         <option value="2">Ganacsi</option>
                         <option value="3">Beerid</option>
                       </select>
                 </div>
              </div>
              <div class="col-lg-4 input_field_sections">
               <h5>Town</h5>
               <div class="form-group">
                   <select name="town" class="form-control">
                     <option value="0">Select Town</option>
                       <option value="1">Israac</option>
                       <option value="2">12</option>
                       <option value="3">12</option>
                     </select>
               </div>
            </div>
            <div class="col-lg-4 input_field_sections">
             <h5>Line</h5>
             <div class="form-group">
                 <select name="line" class="form-control">
                   <option value="0">Select Line</option>
                     <option value="1">Laanta 1</option>
                     <option value="2">Laanta 2</option>
                     <option value="3">Laanta 3</option>
                     <option value="4">Laanta 4</option>
                     <option value="5">Laanta 5</option>
                     <option value="6">Laanta 6</option>
                   </select>
             </div>
          </div>
              </div>
              <!-- end of 1st row -->

              <!--second row  -->
              <div class="row">
                  <div class="col-lg-12">
                    <div class="row">
                    <div class="col-lg-6 input_field_sections">
                       <h5>Zone</h5>
                       <div class="form-group">
                         <input class="form-control" type="text" name="zone" value="">
                       </div>
                    </div>
                    <div class="col-lg-6 input_field_sections">
                     <h5>Area of Land</h5>
                     <div class="form-group">
                       <input class="form-control" type="number" name="area" value="">
                     </div>
                  </div>
                    </div>



                    <!-- end of row 2 -->
                    <!-- third row -->
                    <div class="row">
                      <div class="col-lg-3 input_field_sections">
                       <h5>North</h5>
                       <div class="form-group">
                         <input class="form-control" type="text" name="north" value="">
                       </div>
                     </div>
                     <div class="col-lg-3 input_field_sections">
                      <h5>West</h5>
                      <div class="form-group">
                        <input class="form-control" type="text" name="west" value="">
                      </div>
                    </div>
                    <div class="col-lg-3 input_field_sections">
                     <h5>East</h5>
                     <div class="form-group">
                       <input class="form-control" type="text" name="east" value="">
                     </div>
                   </div>
                   <div class="col-lg-3 input_field_sections">
                    <h5>South</h5>
                    <div class="form-group">
                      <input class="form-control" type="text" name="south" value="">
                    </div>
                  </div>
                    </div>
                    <!--end of row 3  -->
                    <!-- start of row 4 -->
                    <div class="row">
                      <div class="col-lg-4 input_field_sections">
                        <h5>Witness one</h5>
                        <div class="form-group">
                          <input class="form-control" type="text" name="witness1" value="">
                      </div>
                    </div>
                    <div class="col-lg-4 input_field_sections">
                      <h5>Witness two</h5>
                      <div class="form-group">
                        <input class="form-control" type="text" name="witness2" value="">
                    </div>
                  </div>
                  <div class="col-lg-4 input_field_sections">
                    <h5>Witness three</h5>
                    <div class="form-group">
                      <input class="form-control" type="text" name="witness3" value="">
                  </div>
                  </div>
              </div>
              <!--end of row 4 -->
              <input class="btn btn-primary offset-10" type="submit" value="Save">

            </form>

        </div>
      </div>


    </div>
    <!-- end of class 12 -->
    </div>

@stop
@section('footer_scripts')
    <!--  plugin scripts -->
    <script type="text/javascript" src="{{asset('assets/vendors/countUp.js/js/countUp.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flip/js/jquery.flip.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pluginjs/jquery.sparkline.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/chartist/js/chartist.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pluginjs/chartist-tooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/swiper/js/swiper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/circliful/js/jquery.circliful.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.js')}}" ></script>
    <script type="text/javascript" src="{{asset('assets/vendors/flotchart/js/jquery.flot.resize.js')}}"></script>
    <!--end of plugin scripts-->

    <script type="text/javascript" src="{{asset('assets/js/pages/index.js')}}"></script>
@stop
