<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="UTF-8">
    <title>
        @section('title')
            Galkacy LTMS
        @show
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('assets/img/logo.ico')}}"/>
    <!-- global styles-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />

    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/components.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/custom.css')}}"/>
    <link type="text/css" rel="stylesheet" href="#" id="skin_change"/>
    <!-- end of global styles-->
    <style media="screen">
    body {
      /*: url(../../img/login.jpg)*/
        background-color: #4189dd;
        background-repeat: no-repeat;
        background-size: cover;
        background-position:center;
        color: white;
        padding-top:100px;

    }
    .top-buffer { margin-top:50px; }

    .container{
      background-color: #4189dd;
      background-size: cover;
      color: white;
      background-position: center center;
    }

    .img{
      width: 130px;
      height: 150px;
    }
   
h4{
  color: white;
}

.login ul.notice {
  margin: 0;
  border: 0;
  width: 100%;
  text-align: center;
  list-style-position: inside;
  list-style: none;
  font-weight: 600;
  font-size: 15px;
  padding: 10px;
}
/* line 42, /app/app/assets/stylesheets/v2/login/login.scss */
.login ul.notice.errors {
  background-color: #EDE04D;
  color: #212200;
}
/* line 46, /app/app/assets/stylesheets/v2/login/login.scss */
.login ul.notice.success {
  background-color: #15CD72;
  color: white;
}
/* line 50, /app/app/assets/stylesheets/v2/login/login.scss */
.login ul.notice li {
  padding: 5px 0;
}
/* line 55, /app/app/assets/stylesheets/v2/login/login.scss */
.login p.registration-message {
  font-size: 22px;
  line-height: 28px;
  font-weight: 400;
  text-align: center;
  color: white;
  margin: 15px 0 40px;
}
.vertical-form {
  margin: 0 auto;
  width: auto;
  padding: 30px;
  border-radius: 3px;
  background: white;
}

.vertical-form legend {
  text-align: center;
  font-weight: 300;
  color: #444;
  margin: 0 auto 35px;
  font-size: 20px;
  line-height: 38px;
  text-transform: none;
  letter-spacing: 0;
}

.vertical-form legend:after {
  display: none;
}

    </style>

</head>
<!-- tets -->

 @if ($errors->has('username'))

<body class="login">
    <ul class="notice errors">
          <span class="badge badge-secondary">
              <li class="notice"> <h4 style="color: black">{{ $errors->first('username') }}</h4></li>
          </span>
</ul>
      @endif

  
 
<div class="container">
<div class="row top-buffer">
<div class="col-lg-4 offset-md-4">

<h4 class="text-center"><img class="img" src="{{asset('assets/img/logo.png')}}" alt="logo"></h4>
    <h4 class="text-center">Galkacyo Land Tenure Database Management System</h4>

  <form class="form-horizontal vertical-form" method="POST" action="{{ route('login') }}">
      {{ csrf_field() }}
    <legend>Login</legend>

    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
<!--       <label for="username" class="control-label">Username</label>
 -->      
 <input id="username" type="text" class="form-control form-control-lg" name="username" value="{{ old('username') }}" placeholder="Username" required autofocus>
      
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
<!--       <label for="password" class="control-label">Password</label>
 -->      <input id="password" placeholder="Password" type="password" class="form-control form-control-lg" name="password" required>

    </div>
    <div class="form-group">

      <button class="btn btn-lg btn-success btn-block form-rounded" type="submit">Sign in</button>
    </div>

    </form>
  </div>
</div>
</div>

<!-- global scripts-->
<!-- the first one is jquery -->
<script type="text/javascript" src="{{asset('assets/js/components.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script>
<script>


<!-- end of global scripts-->
<!-- page level js -->
@yield('footer_scripts')
<!-- end page level js -->
</body>
</html>
