@extends('layouts/default')
{{-- Page title --}}
@section('title')
    Dashboard
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')

<!--Plugin css-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/fullcalendar/css/fullcalendar.min.css')}}"/>
<!--End off plugin css-->
<!--Page level css-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/timeline2.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/calendar_custom.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/profile.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/gallery.css')}}"/>
<!--end of page level css-->

@stop


{{-- Page content --}}
@section('content')
<header class="head">
       <div class="main-bar row">
           <div class="col-lg-6">
               <h4 class="nav_top_align skin_txt">
                   <i class="fa fa-user"></i>
                   User Profile
               </h4>
           </div>
           <div class="col-lg-6">

           </div>
       </div>
   </header>
   <div class="outer">
       <div class="inner bg-container">
           <div class="card">
               <div class="card-block">
                   <div class="row">
                       <div class="col-lg-6 m-t-35">
                           <div class="text-xs-center">
                               <div class="form-group">
                                   <div class="fileinput fileinput-new" data-provides="fileinput">
                                       <div class="fileinput-new thumb_zoom zoom admin_img_width">
                                         <form class="" action="{{route('photoupdate', $user->id)}}" method="post" enctype="multipart/form-data">
                                           {{ csrf_field() }}
                                           {{method_field('PUT')}}


                                         <div class="fileinput-new thumb_zoom zoom admin_img_width">
                                        <img src="/storage/{{$user->photo}}" alt="admin" class="admin_img_width"></div>                                       </div>
                                       <div class="fileinput-preview fileinput-exists thumb_zoom zoom admin_img_width"></div>
                                       <div class="btn_file_position">
                                                   <span class="btn btn-primary btn-file">
                                                       <span class="fileinput-new">Change image</span>
                                                       <span class="fileinput-exists">Change</span>
                                                       <input type="file" name="Changefile">
                                                   </span>
                                           <a href="#" class="btn btn-warning fileinput-exists"
                                              data-dismiss="fileinput">Remove</a>
                                              <input type="submit" name="" class="btn btn-warning fileinput-exists" value="Update">

                                       </div>
                                     </form>
                                     </div>
                               </div>
                           </div>

                       </div>
                       <div class="col-lg-6 m-t-25">
                           <div>
                               <h1>User Details</h1>
                                       <table class="table" id="users">
                                           <tr>
                                               <td>Full Name</td>
                                               <td class="inline_edit">
                                                       <span class="editable"
                                                             data-title="Edit User Name">{{ $user->name }}</span>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>E-mail</td>
                                               <td>
                                                   <span class="editable" data-title="Edit E-mail">{{ $user->email }}</span>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>Gender</td>
                                               <td>
                                                   <span class="editable" data-title="Edit Phone Number">{{ $user->gender }}</span>
                                               </td>
                                           </tr>

                                           <tr>
                                               <td>Created At</td>
                                               <td>{{ $user->created_at }}</td>
                                           </tr>
                                           <tr>
                                               <td>position</td>
                                               <td>
                                                   <span class="editable" data-title="Edit City">{{ $user->position }}</span>
                                               </td>
                                           </tr>

                                       </table>
                                   </div>
<!-- <a href="{{route('changepassword', [$user->id])}}">Chenage password</a> -->

                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>



@stop
@section('footer_scripts')

<!--Plugin scripts-->
<script type="text/javascript" src="{{asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap_calendar/js/bootstrap_calendar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/fullcalendar/js/fullcalendar.min.js')}}"></script>
<!--End of Plugin scripts-->
<!--Page level scripts-->
<script type="text/javascript" src="{{asset('assets/js/pages/mini_calendar.js')}}"></script>
<!--End of Page level scripts-->
@stop
