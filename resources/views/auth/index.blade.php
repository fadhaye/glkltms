@extends('layouts/default')
{{-- Page title --}}
@section('title')
    Dashboard
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}"/>
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>
   <!--End of plugin styles-->
   <!--Page level styles-->
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/>
   <!-- end of page level styles -->

@stop


{{-- Page content --}}
@section('content')

<header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-lg-6 col-sm-4">
                    <h4 class="nav_top_align">
                        <i class="fa fa-user"></i>
                        Users Data
                    </h4>
                </div>
                <div class="col-lg-6 col-sm-8 col-12">
                    <ol class="breadcrumb float-right  nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="index1">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Users</a>
                        </li>
                        <li class="active breadcrumb-item">User Grid</li>
                    </ol>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
      @if (session('status'))
      <div class="alert alert-info">
        <a class="close" data-dismiss="alert">×</a>
              {{ session('status') }}
            </div>
      @endif
      @if(Session::has('msg'))
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Well Done!</strong> {!!Session::get('msg')!!}
        </div>
        @endif
        <div class="inner bg-container">
            <div class="card">
                <div class="card-header bg-white">
                  <i class="fa fa-lock"></i>
                </div>
                <div class="card-block m-t-35" id="user_body">
                    <div class="table-toolbar">
                        <div class="btn-group">
                            <a href="{{route('auth.create')}}" id="editable_table_new" class=" btn btn-default">
                                Add User <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="btn-group float-right users_grid_tools">
                            <div class="tools"></div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <table class="table  table-striped table-bordered table-hover dataTable no-footer"
                                   id="editable_table" role="grid" >
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc wid-10" tabindex="0" rowspan="1" colspan="1">ID</th>
                                    <th class="sorting wid-20" tabindex="0" rowspan="1" colspan="1">Name</th>
                                    <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Email</th>
                                    <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">Roles</th>
                                    <th class="sorting wid-5" tabindex="0" rowspan="1" colspan="1">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @foreach($users as $user)
                                <tr role="row" class="even">
                                    <td class="sorting_1">{{$user->id}}
                                    </td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td class="center">

                                    @foreach ($user->roles as $role)
                                              <li>{{$role->name}}</li> &nbsp;
                                    @endforeach



                                    </td>
                                    <td class="no-sort no-click" id="bread-actions">


                                      <a href="{{route('auth.edit', [$user->id])}}" title="Edit" class="btn btn-sm btn-warning pull-right">
                                      <i class="fa fa-eye"></i> <span class="hidden-xs hidden-sm">Edit   </span>
                                      </a>
                                    <a href="{{route('auth.show', [$user->id])}}" title="View" class="btn btn-sm btn-warning pull-right">
                                    <i class="fa fa-eye"></i> <span class="hidden-xs hidden-sm">View   </span>
                                    </a>

                                    </td>

                                </tr>
                                @endforeach



                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- /.inner -->
    </div>
    <!-- /.outer -->
@stop
@section('footer_scripts')

    <!--Plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('assets/vendors/datatables/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.colVis.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/users.js')}}"></script>
    <!-- end page level scripts -->

@stop
