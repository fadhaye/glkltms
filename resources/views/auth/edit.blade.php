@extends('layouts/app')
{{-- Page title --}}
@section('title')
    Dashboard
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}"/>
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>
   <!--End of plugin styles-->
   <!--Page level styles-->
   <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/>
   <!-- end of page level styles -->

@stop


{{-- Page content --}}
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('auth.update', [$user->id]) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{method_field('PUT')}}




                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Full Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{old('name', $user->name)}}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">UserName</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{old('username', $user->username)}}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                            <label for="dob" class="col-md-4 control-label">Date of Birth</label>

                            <div class="col-md-6">
                                <input id="dob" type="date" class="form-control" name="dob" value="{{old('dob', $user->dob)}}"  required autofocus>

                                @if ($errors->has('dob'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            <label for="gender" class="col-md-4 control-label">Gender</label>

                            <div class="col-md-6">
                                <input type="radio" class="form-check-input"  name="gender" value="male" {{ $user->gender == 'male' ? 'checked' : ''}}> Male<br>
                                <input type="radio" class="form-check-input" name="gender" value="female" {{ $user->gender == 'female' ? 'checked' : ''}}> Female<br>
                                @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email', $user->email) }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                            <label for="position" class="col-md-4 control-label">Position</label>

                            <div class="col-md-6">
                                <select class="form-control" name="position">
                                  <option  value="{{$user->position}}">{{$user->position}}</option>
                                  <option value="TEST 1">TEST 1</option>
                                  <option value="TEST 2">TEST 2</option>
                                  <option value="TEST 3">TEST 3</option>
                                </select>
                                @if ($errors->has('position'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                          <label for="roles" class="col-md-4 control-label">Select User Roles</label>
                          <div class="col-md-6">


                            @foreach($roles as $roles)
                            <input type='checkbox' name="roles[]"value='{{$roles->id}}'
                                    {{$user->hasrole($roles->name) ? 'checked':''}}>
                                    {{$roles->name}}
                            </input>
                              @endforeach

                          </div>
                        </div>

                        <div class="form-group{{ $errors->has('activation') ? ' has-error' : '' }}">
                            <label for="activation" class="col-md-4 control-label">Activation</label>

                            <div class="col-md-6">

                                <input type="radio" class="form-check-input" name="activate" value="1"
                                {{ $user->activate == 1 ? 'checked' : ''}}> Activate<br>
                                <input type="radio" class="form-check-input" name="activate" value="0"
                                {{ $user->activate == 0 ? 'checked' : ''}}> Disactivate<br>
                                @if ($errors->has('activation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('activation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>




                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
