<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('sname');
            $table->string('tname');
            $table->string('fthname');
            $table->string('mother');
            $table->integer('pid');
            $table->string('type');
            $table->date('dob');
            $table->string('town');
            $table->string('line');
            $table->string('zone');
            $table->string('photo');
            $table->string('gender');
            $table->string('phone');
            $table->string('email')->nullable();;
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
