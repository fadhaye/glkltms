<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('town');
            $table->string('laanta');
            $table->string('zone');
            $table->double('length');
            $table->double('width');
            $table->double('areaofland');
            $table->string('north');
            $table->string('south');
            $table->string('east');
            $table->string('west');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('witness1');
            $table->string('witness2');
            $table->string('witness3');
            $table->integer('user_id');
            $table->integer('owner_id')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lands');
    }
}
