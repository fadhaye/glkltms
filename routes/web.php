<?php

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

//what
Route::resource('/auth', 'UserController');

Route::get('/auth/{auth}/chenagepassword', 'UserController@changepassword')->name('changepassword');
Route::put('/auth/{auth}/updatepassword', 'UserController@updatepassword')->name('updatepassword');

Route::put('/auth/{auth}/photoupdate', 'UserController@photoupdate')->name('photoupdate');


Route::get('/errors', 'HomeController@error403')->name('errors.403');
Route::get('/', 'HomeController@welcome')->name('welcome');
Route::get('/welcome', 'HomeController@welcome')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('search', 'OwnerController@search')->name('search');


Route::get('/log', 'ActivitylogController@index')->name('activitylog');

Route::get('/lands/createland', 'LandController@createland')->name('lands.createland');
Route::get('/lands/transfer', 'LandController@transfer')->name('lands.transfer');

Route::get('/lands/payment', 'LandController@payment')->name('lands.payment');


Route::resource('owners', 'OwnerController');
Route::resource('lands', 'LandController');


Route::get('/lands.maps', 'LandController@maps')->name('lands.maps');
Route::get('/lands.addmaps', 'LandController@addmaps')->name('lands.addmaps');
