<!DOCTYPE html>
<html>

<head>
  @include('partials.head')
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    @include('partials.header')

  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    @include('partials.aside')

  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create New Owner Registeration
      </h1>

    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
                <div class="col-md-12">


                        <h3 class="page-title">Users</h3>
    <form method="POST" action="{{ route('owners.store') }}" accept-charset="UTF-8"><input name="_token" type="hidden" value="ICPFxCa5fhWmIugl7aG2x0mSsYaFlSu0QAFW0D3N">
      {{ csrf_field() }}

    <div class="panel panel-default">
        <div class="panel-heading">
            Create        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <label for="fname" class="control-label">First Name</label>
                    <input class="form-control" placeholder="" required="" name="fname" type="text" id="fname">
                    <p class="help-block"></p>
                </div>
                </div>

              <div class="row">
                    <div class="col-xs-12 form-group">
                        <label for="sname" class="control-label">Second Name</label>
                        <input class="form-control" placeholder="" required="" name="sname" type="text" id="sname">
                        <p class="help-block"></p>
                    </div>
              </div>
              <div class="row">
                    <div class="col-xs-12 form-group">
                        <label for="tname" class="control-label">Third Name</label>
                        <input class="form-control" placeholder="" required="" name="tname" type="text" id="tname">
                        <p class="help-block"></p>
                    </div>
              </div>
              <div class="row">
                    <div class="col-xs-12 form-group">
                        <label for="pid" class="control-label">Puntland ID</label>
                        <input class="form-control" placeholder="989876" required="" name="pid" type="number" id="pid">
                        <p class="help-block"></p>
                    </div>
              </div>

              <div class="row">
                    <div class="col-xs-12 form-group">
                        <label for="address" class="control-label">Current Address</label>
                        <input class="form-control" placeholder="address" required="" name="address" type="text" id="address">
                        <p class="help-block"></p>
                    </div>
              </div>
              <div class="row">
                    <div class="col-xs-12 form-group">
                        <label for="dob" class="control-label">Date of Birth</label>
                        <input class="form-control" placeholder="address" required="" name="dob" type="date" id="dob">
                        <p class="help-block"></p>
                    </div>
              </div>
              <div class="row">
                    <div class="col-xs-12 form-group">
                        <label for="photo" class="control-label">photo</label>
                        <input class="form-control" placeholder="address" required="" name="photo" type="file" id="photo">
                        <p class="help-block"></p>
                    </div>
              </div>
              <div class="row">
                    <div class="col-xs-12 form-group">
                        <label for="phone" class="control-label">Phon Number</label>
                        <input class="form-control" placeholder="090-6666666" required="" name="phone" type="number" id="phone">
                        <p class="help-block"></p>
                    </div>
              </div>


            <div class="row">
                <div class="col-xs-12 form-group">
                    <label for="email" class="control-label">Email</label>
                    <input class="form-control" placeholder=""  name="email" type="email" id="email">
                    <p class="help-block"></p>
                                    </div>
            </div>





        </div>
    </div>

    <input class="btn btn-danger offset-6" type="submit" value="Save">
    </form>

                </div>
            </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    @include('partials.footer')

  </footer>


<!-- REQUIRED JS SCRIPTS -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/adminlte.min.js') }}"></script>

</body>
</html>
