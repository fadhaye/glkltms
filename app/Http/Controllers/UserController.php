<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;
use App\owner;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    public function __construct()
       {
           $this->middleware('role:superadministrator|admin|owner');
       }

    public function index()
      {
          $users = user::all();
          return view('auth.index', compact('users'));
      }

    public function create()
    {
        $roles =role::all();
        return view('auth.register', compact('roles'));
    }

    public function store(Request $request)
    {
      //dd(Request()->all());
        $request->validate([
        'name' => 'required|string|max:255',
        'username' => 'required|string|unique:users',
        'photo' => 'required|image',
        'dob' => 'required|date|after:01.01.1915|before:today',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6|confirmed',
      ]);

      $user = new user;
      $user->name = request('name');
      $user->photo = request('photo');
      $user->username = request('username');
      $user->gender = request('gender');
      $user->position = request('position');
      $user->dob = request('dob');
      $user->email = request('email');
      $user->password = bcrypt(request('password'));
      $path = $request->photo->store('users');
      $store = $request->photo->store('public/users');
      $user->photo = $path;
      $user->save();
      $user->roles()->attach($request->input('roles') === null ? [] : $request->input('roles'));
      Session()->flash('status', 'success user created successfully!');
      return redirect('/auth');

    }

//Auth::user()->hasRole('superadministrator') && Auth::id()==$user->id)
    public function show($id)
      {
        $user =user::findOrFail($id);
        return view('auth.show', compact('user'));
      }

    public function edit($id)
    {
      $roles =role::all();
      $user =user::findOrFail($id);
       //$user = user::where('id', '$id')->with('roles')->first();
      return view('auth.edit', compact('roles','user'));
    }

    public function update(Request $request, $id)
    {
      $user = user::findOrFail($id);
      // dd(Request()->all());
        $request->validate([
        'name' => 'required|string|max:255',
        'dob' => 'required|date|after:01.01.1915|before:today',
        'username'=>'required|unique:users,username,'.$user->id
      ]);
      $user->name = request('name');
      $user->username = request('username');
      $user->gender = request('gender');
      $user->position = request('position');
      $user->dob = request('dob');
      $user->activate = request('activate');
      $user->email = request('email');
      $user->save();
      $user->roles()->detach();
      $user->roles()->attach($request->input('roles') === null ? [] : $request->input('roles'));
      Session()->flash('msg', 'success user updated successfully!');
      return redirect('/auth');
    }

    public function changepassword($id)
    {
      $roles =role::all();
      $user =user::findOrFail($id);
      return view('auth.changepassword', compact('roles','user'));
    }

    public function updatepassword(Request $request, $id)
    {
       // dd(Request()->all());
         $request->validate([
         'current' => 'required|string|max:255',
         'password' => 'required|string|min:6|confirmed',
       ]);

        $user = user::findOrFail($id);
        $oldpass= $user->password;
        $newpass = request('current');
        if (Hash::check($newpass, $oldpass)) {
            $user->password = bcrypt(request('password'));
            $user->save();
            Session()->flash('msg', 'the password has been updated successfully!');
            return redirect('logout');
        }else {
      Session()->flash('msg', 'please check your current password');
      return redirect()->route('changepassword', ['id' => $id]);

    }}

    public function photoupdate(Request $request, $id)
    {
       // dd(Request()->all());
      //    $request->validate([
       //
      //  ]);

        $user = user::findOrFail($id);
        $path = $request->Changefile->store('users');
        $store = $request->Changefile->store('public/users');
        $user->photo = $path;
        $user->save();

        return redirect()->route('auth.show', ['id' => $id]);

    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
