<?php

namespace App\Http\Controllers;

use App\land;
use App\owner;
use App\User;
use App\payment;
use Auth;

use Illuminate\Http\Request;

class LandController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
       $this->middleware('role:superadministrator|admin|lands');
     }
    public function index()
    {
        //dd(Request()->all());

        //$lands =land::all();
        $owners =owner::all();
        $lands =land::with('user', 'owner')->take(10)->latest()->get();
        return view('lands.index', compact('lands','owners'));
    }

    public function payment()
    {
      $lands =land::all();
      $owners =owner::all();
        return view('lands.payment', compact('lands', 'owners'));
    }

    public function search(Request $request)
        {

            $term = $request->term;
            $owners = owner::where('fname', 'LIKE', '%'.$term.'%')->get();
            //return $owner;
            if(count($owners) == 0){
                $searchResult[] = 'No owners found !';
            }else{
                foreach($owners as $owner){
                    $searchResult[] = $owner->searchItem;
                }
            }
            return $searchResult;
          }




    public function transfer()
    {

        $lands =land::with('user', 'owner')->take(10)->latest()->get();
        return view('lands.transfer', compact('lands'));
    }


    public function maps()
    {
      //dd(Request()->all());

        $lands =land::all();
        return view('lands.maps', compact('lands'));
    }
    public function addmaps()
    {
        $lands =land::all();
        return view('lands.addmaps', compact('lands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request)
    {
      $id= request('id');
      $owner =owner::findOrFail($id);
      return view('lands.create', compact('owner'));

    }

    public function createland(request $request)
    {
      return view('lands.createland', compact('owner'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(Request()->all());
        $land = new land;
        $land->type =  request('type');
        $land->town =  request('town');
        $land->laanta =  request('line');
        $land->zone =  request('zone');
        $land->length =  request('length');
        $land->width =  request('width');
        $land->areaofland =request('length')*request('width');
        $land->latitude =  request('latitude');
        $land->longitude =  request('longitude');
        $land->north = request('north');
        $land->west =  request('west');
        $land->east =  request('east');
        $land->south = request('south');
        $land->witness1 =request('witness1');
        $land->witness2 =request('witness2');
        $land->witness3 =request('witness3');
        //$land->owner_id =request('owner_id');
        $land->user_id =Auth::id();
        $land->save();
        return redirect('/lands');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\land  $land
     * @return \Illuminate\Http\Response
     */
    public function show(land $land)
    {

         $lands = land::all();
         return view('lands.show', compact('land', 'owner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\land  $land
     * @return \Illuminate\Http\Response
     */
    public function edit(land $land)
    {
      $id= request('id');
      $owner =owner::find($id);
      $transfers =owner::all();
      return view('lands.edit', compact('land','owner','transfers'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\land  $land
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, land $land)
    {
          //dd(Request()->all());
          $id= request('id');
          $owner =owner::find($id);
          $land->type =  request('type');
          $land->town =  request('town');
          $land->laanta =  request('line');
          $land->zone =  request('zone');
          $land->length =  request('length');
          $land->width =  request('width');
          $land->areaofland =request('length')*request('width');
          $land->latitude =  request('latitude');
          $land->longitude =  request('longitude');
          $land->north = request('north');
          $land->west =  request('west');
          $land->east =  request('east');
          $land->south = request('south');
          $land->witness1 =request('witness1');
          $land->witness2 =request('witness2');
          $land->witness3 =request('witness3');
          $land->owner_id =request('owner_id');
          $land->user_id =Auth::id();
          $land->save();
          return redirect('/lands');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\land  $land
     * @return \Illuminate\Http\Response
     */
    public function destroy(land $land)
    {
        //
    }
}
