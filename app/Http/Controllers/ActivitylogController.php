<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Models\Activity;
class ActivitylogController extends Controller
{
    //
    use LogsActivity;

    public function __construct()
    {
      $this->middleware('role:superadministrator');
    }
    public function index()
       {

          $logItems = Activity::all();
           //return view('auth/log/index', compact('logItems'));
           //return __METHOD__;
           return view('log.index', compact('logItems'));
       }


 }
