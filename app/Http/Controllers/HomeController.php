<?php

namespace App\Http\Controllers;
use App\owner;
use App\User;
use App\land;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //dd(Request()->all());
        $lands = land::with('owner')->get();
        $owners =owner::all();
        $countmale = owner::where('gender','=','male')->count();
        $countfemale = owner::where('gender','=','female')->count();

        $countisraac = land::where('town','=','israac')->count();
        $countgarsoor = land::where('town','=','garsoor')->count();
        $countbarwaaqo = land::where('town','=','barwaaqo')->count();


        $countDeedaan = land::where('type','=','Deegaan')->count();
        $countGanacsi = land::where('type','=','Ganacsi')->count();
        $countBeerid = land::where('type','=','Beerid')->count();

        return view('home',compact('lands', 'owners','countmale',
              'countfemale', 'countisraac', 'countgarsoor', 'countbarwaaqo',
              'countDeedaan', 'countGanacsi','countBeerid'));
    }

    public function welcome()
    {
      //dd(Request()->all());
        $lands = land::with('owner')->get();
        $owners =owner::all();
        $countmale = owner::where('gender','=','male')->count();
        $countfemale = owner::where('gender','=','female')->count();

        $countisraac = land::where('town','=','israac')->count();
        $countgarsoor = land::where('town','=','garsoor')->count();
        $countbarwaaqo = land::where('town','=','barwaaqo')->count();


        $countDeedaan = land::where('type','=','Deegaan')->count();
        $countGanacsi = land::where('type','=','Ganacsi')->count();
        $countBeerid = land::where('type','=','Beerid')->count();

        return view('welcome',compact('lands', 'owners','countmale',
              'countfemale', 'countisraac', 'countgarsoor', 'countbarwaaqo',
              'countDeedaan', 'countGanacsi','countBeerid'));
    }
    public function error403()
    {
        return view('errors.403');
    }
}
