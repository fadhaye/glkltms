<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use App\owner;
use App\User;
use App\land;
use Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use DB;
use Illuminate\Http\Request;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function __construct()
     {
       $this->middleware('role:superadministrator|admin|owners');
     }


    public function index()
    {
        $owners =owner::latest()->get();;
        return view('owners.index', compact('owners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      return view('owners.create');

    }
    public function search(Request $request)
    {

        $term = $request->term;

        $owners = owner::where('fname','LIKE', '%'.$term.'%')
                        ->orwhere('sname', 'LIKE', '%'.$term.'%')
                        ->orwhere('pid', 'LIKE', '%'.$term.'%')
                        ->select(DB::raw(
                          'pid, fname,
                           sname'

                        ))->get();


        // $owners1 = owner::where('fname', 'LIKE', '%'.$term.'%')->get();

        if(count($owners) == 0){
        $search[] = 'No owner found !';
        }else {
          foreach($owners as $owner){
            $pid= $owner->pid;
            $fname= $owner->fname;
            $sname= $owner->sname;
            $owner= $pid.": ".$fname." ".$sname; 
            $search[] = $owner;

          }

          }

          return $search;


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(request $request)
    {
          $validatedData = $request->validate([
           'photo' => 'required|image',
           'fname' => 'required|string|max:25',
           'sname' => 'required|string|max:25',
           'tname' => 'required|string|max:25',
           'fthname' => 'required|string|max:25',
           'tname' => 'required|string|max:25',
           'pid' => 'required|string|max:15|unique:owners',
           'type' => 'required|string|max:15|',
           'town' => 'required|string|max:50',
           'line' => 'required|string|max:50',
           'zone' => 'required|string|max:50',
           'dob' => 'required|date|after:01.01.1915|before:today',
           'gender' => 'required|string|max:8',
           'phone' => 'required|string|max:12',
           'email' => 'nullable|email|max:255|unique:owners',
        ]);
          //dd(Request()->all());
          $owner = new owner;
          $owner->fname = request('fname');
          $owner->sname = request('sname');
          //$owner->sname = encrypt(request('sname'));
          $owner->tname = request('tname');
          $owner->fthname = request('fthname');
          $owner->mother = request('mother');
          $owner->pid = request('pid');
          $owner->type = request('type');
          $owner->dob = request('dob');
          $owner->town = request('town');
          $owner->line = request('line');
          $owner->zone = request('zone');
          $owner->email = request('email');
          $owner->phone = request('phone');
          $owner->gender = request('gender');


              $path = $request->photo->store('photos');
              $store = $request->photo->store('public/photos');
              $owner->photo = $path;


          $owner->user_id =Auth::id();
          $owner->save();
          //return redirect('/owners');
          return redirect()->route('owners.index')->with('success','owner Created');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function show(owner $owner)
    {
      //$lands =land::with('owner')->get();
      $owners = owner::with('lands')->get();
      return view('owners.show', compact('owner', 'land'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function edit(owner $owner)
    {
      return view('owners.edit', compact('owner'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      //dd(Request()->all());
      //$id= request('id');
      $owner =owner::findOrFail($id);
      $owner->fname = request('fname');
      $owner->sname = request('sname');
      $owner->tname = request('tname');
      $owner->pid = request('pid');
      $owner->dob = request('dob');
      $owner->address = request('address');
      $owner->email = request('email');
      $owner->phone = request('phone');
      $owner->gender = request('gender');
      $owner->photo = request('photo');
      $owner->user_id =Auth::id();
      $owner->save();
      return redirect('/owners');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function destroy(owner $owner)
    {
        //
    }
}
