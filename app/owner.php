<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class owner extends Model
{


  use LogsActivity;

  protected $fillable = [
    'fname','sname','tname','address','photo','gender','phone','pid', 'email', 'dob','user_id',
                        ];

  protected static $logFillable = true;

  public function getLogNameToUse(string $eventName = ''): string
      {
         return 'Owner';
      }

      public function getDescriptionForEvent(string $eventName): string
    {

      $model ='owner Management';

      return "The $model model has been {$eventName}";

    }

          public function user()
           {
               return $this->belongsTo(user::class);
           }


           public function lands()
            {
             return $this->hasMany(land::class);
            }



}
