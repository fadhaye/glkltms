<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Spatie\Activitylog\Traits\LogsActivity;
//use Laravel\Scout\Searchable;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;
    use LogsActivity;
  //  use Searchable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','photo', 'gender', 'position', 'dob',
    ];
    public function getLogNameToUse(string $eventName = ''): string
        {
           return 'user';
        }
        public function getDescriptionForEvent(string $eventName): string
      {
          $model ='User Management';

          return "The $model model has been {$eventName}";
      }

    protected static $logFillable = true;





    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

     public function owners()
          {
           return $this->hasMany(owner::class);
          }
          // public function roles()
          //      {
          //       return $this->belongsToMany(role::class);
          //      }
      public function lands()
                   {
                       return $this->belongsTo(lands::class);
                   }
    protected $hidden = [
        'password', 'remember_token',
    ];
}
