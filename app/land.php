<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class land extends Model
{


    use LogsActivity;

  protected $fillable = [
    'type', 'town', 'laanta', 'zone', 'width','length', 'areaofland', 'latitude', 'longitude', 'north', 'west',
'east', 'south', 'witness1', 'witness2', 'witness3', 'owner_id', 'user_id',
  ];

  protected static $logFillable = true;

  public function getLogNameToUse(string $eventName = ''): string
  {
     return 'Land';
  }
    public function getDescriptionForEvent(string $eventName): string
  {
        $model ='land Management';

          return "The $model model has been {$eventName}";
  }


        public function owner()
         {
             return $this->belongsTo(owner::class);
         }
         public function user()
                  {
                      return $this->belongsTo(user::class);
                  }
}
