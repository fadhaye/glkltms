<?php

namespace App;

use Laratrust\Models\LaratrustRole;
use Spatie\Activitylog\Traits\LogsActivity;

class Role extends LaratrustRole
{
    //
    use LogsActivity;

    // public function users()
    //  {
    //      return $this->belongsToMany(user::class);
    //  }

    protected static $logFillable = true;


}
